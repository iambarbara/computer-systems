/*
 * esh - the 'pluggable' shell.
 *
 * Developed by Godmar Back for CS 3214 Fall 2009
 * Virginia Tech.
 */
#include <stdio.h>
#include <readline/readline.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <errno.h>

#include "esh.h"
#include "esh-sys-utils.h"
#include "list.h"

static int file_mode_mask = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;

static void free_jobs(struct list * jobs);
static void configure_pipeline(struct esh_pipeline *pipeline);
static void setup_and_run_cmd(struct  esh_command *cmds, int* in_fd, int* out_fd);
static void launch_process(struct  esh_command *cmds, int* in_fd, int* out_fd);
static void * sig_chld_handler (int sig, siginfo_t * sig_info, void * data);
static void put_job_in_foreground(struct esh_pipeline *job, bool cont, int pid);
static void put_job_in_background(struct esh_pipeline *job, bool cont);
static int mark_process_status(pid_t pid, int status);
static void give_tty_to_pgrp(pid_t pgrp, struct termios *pg_tty_state, struct termios *old_tty_state);
//static void update_status(void);
static void wait_for_job(struct esh_pipeline *job);
static void print_job_status(struct esh_command *cmd);

static void
usage(char *progname)
{
    printf("Usage: %s -h\n"
        " -h            print this help\n"
        " -p  plugindir directory from which to load plug-ins\n",
        progname);

    exit(EXIT_SUCCESS);
}

/* Build a prompt by assembling fragments from loaded plugins that 
 * implement 'make_prompt.'
 *
 * This function demonstrates how to iterate over all loaded plugins.
 */
static char *
build_prompt_from_plugins(void)
{
    char *prompt = NULL;
    struct list_elem * e = list_begin(&esh_plugin_list);

    for (; e != list_end(&esh_plugin_list); e = list_next(e)) {
        struct esh_plugin *plugin = list_entry(e, struct esh_plugin, elem);

        if (plugin->make_prompt == NULL)
            continue;

        /* append prompt fragment created by plug-in */
        char * p = plugin->make_prompt();
        if (prompt == NULL) {
            prompt = p;
        } else {
            prompt = realloc(prompt, strlen(prompt) + strlen(p) + 1);
            strcat(prompt, p);
            free(p);
        }
    }

    /* default prompt */
    if (prompt == NULL)
        prompt = strdup("esh> ");

    return prompt;
}

//-----------------------------------------------------------------------------

/* 
 * Returns a list of current jobs.
 */

static struct list * get_jobs(void)
{
	return &current_jobs;
}

/*
 * Returns the job corresponding to the input jid, else NULL.
 */
static struct esh_pipeline * get_job_from_jid(int jid)
{

	struct list_elem *e = list_begin(&current_jobs);
	while (e != list_end(&current_jobs))
	{
		struct esh_pipeline *job = list_entry(e, struct esh_pipeline, elem);
		if (job->jid == jid) return job;
		e = list_next(e);
	}

	return NULL;
}

/*
 * Returns job corresponding to the pgrp, else NULL.
 */
static struct esh_pipeline * get_job_from_pgrp(pid_t pgrp)
{
	struct list_elem *e = list_begin(&current_jobs);
	while (e != list_end(&current_jobs))
	{
		struct esh_pipeline *job = list_entry(e, struct esh_pipeline, elem);
		if (job->pgrp == pgrp) return job;
		e = list_next(e);
	}

	return NULL;
}

/*
 * Returns command corresponding to the pid, else NULL.
 */
static struct esh_command * get_cmd_from_pid(pid_t pid)
{
	struct list_elem *e = list_begin(&current_jobs);
	while (e != list_end(&current_jobs))
	{
		struct esh_pipeline *pl = list_entry(e, struct esh_pipeline, elem);
		struct list_elem * elem = list_begin(&pl->commands);
		while(elem != list_end(&pl->commands)) {
			struct esh_command * cmd = list_entry(elem, struct esh_command, elem);
			if(cmd->pid == pid) return cmd;
			elem = list_next(elem);
		}
		e = list_next(e);
	}
	return NULL;
}

/*
static void update_status(void)
{
	int status;
	pid_t pid;
	
	while ((pid = waitpid(WAIT_ANY, &status, WUNTRACED|WNOHANG))>0)
		mark_process_status(pid, status);
}*/

static int mark_process_status(pid_t pid, int status)
{
	if (pid > 0)
	{
		struct esh_command *cmd = get_cmd_from_pid(pid);

		if (WIFSTOPPED(status)){
			cmd->status = STOPPED;
			cmd->pipeline->status = STOPPED;
			printf("\n");
			print_job_status(cmd);
			
			//return_control_to_esh();
			return 0;
		}
		if (WIFEXITED(status)){
			cmd->status = COMPLETED;
			if(&cmd->elem == list_rbegin(&cmd->pipeline->commands))
				cmd->pipeline->status = COMPLETED;
			
			if(cmd->pipeline->status == COMPLETED) {
				list_remove(&cmd->pipeline->elem);
				if(cmd->pipeline->bg_job) {
					print_job_status(cmd);
				}
				if(list_empty(&current_jobs)) {
					jid = 1;
					curr_job = prev_job = jid;
				} else if(cmd->pipeline->jid + 1 == jid) {
					jid --;
					curr_job = prev_job;
					prev_job = list_entry(list_rbegin(&current_jobs), struct esh_pipeline, elem)->jid;
				}
			}
			return 0;
		}
		return -1;

	}
	else if (pid == 0 || errno == ECHILD) // No processes to report
		return -1;
	else {
		esh_sys_error("waitpid: ");
		return -1;
		}
}

static void print_job_status(struct esh_command *cmd)
{
	if (cmd->status != COMPLETED) {
		int job_id = cmd->pipeline->jid;
		printf("[%d]",job_id);
	
		if (job_id == curr_job) printf("+ ");
		else if (job_id == prev_job) printf("- ");
		else printf("  "); 	
	
		switch (cmd->status){
			case STOPPED:
				printf("Stopped");
				break;
			case NEEDSTERMINAL:
				printf("Needs Terminal");
				break;
			case BACKGROUND:
				printf("Running");
				break;
			case FOREGROUND:
				printf("Running");
				break;
			case COMPLETED:
				printf("Exited");
				break;}
		printf("\t\t(%s", cmd->argv[0]);
		char** arg = cmd->argv;
		arg++;
		while(*(arg) != 0) {
			printf(" %s", *arg);
			arg++;
		}
		printf(")\n");
	}
}

/* saves tty state to old_tty_state, gives control of tty to pgrp, restores pg_tty_state */
void give_tty_to_pgrp(pid_t pgrp, struct termios *pg_tty_state, struct termios * old_tty_state) {
	esh_signal_block(SIGTTOU);
	if (old_tty_state) esh_sys_tty_save(old_tty_state);
	esh_sys_call(tcsetpgrp(esh_sys_tty_getfd(), pgrp), false, "tcsetpgrp: ");
	if (pg_tty_state) esh_sys_tty_restore(pg_tty_state);
	esh_signal_unblock(SIGTTOU);

	//esh_sys_call(tcsetpgrp(esh_sys_tty_getfd(), getpid()), false, "tcsetpgrp: ");
	//esh_sys_tty_restore(tty_state);
}

void * sig_chld_handler (int sig, siginfo_t * sig_info, void * data) {
	//esh_sys_call(tcsetpgrp(esh_sys_tty_getfd(), getpid()), "tcsetpgrp: ", true);
	//esh_sys_tty_restore(tty_state);
	int err = errno;
#ifdef _DEBUG
	printf("SIGCHLD\n");
#endif
	int stat = 0;
	if(waitpid(sig_info->si_pid, &stat, WUNTRACED | WNOHANG) < 0) {
		if(errno != ECHILD) {
			esh_sys_fatal_error("waitpid: ");
		}
		give_tty_to_pgrp(getpgrp(), tty_state, NULL);
	}
#ifdef _DEBUG
	else if(WIFSTOPPED(stat)) {
		if(WSTOPSIG(stat) | (SIGTTIN|SIGTTOU)){
			printf("Needs terminal\n");
		}
	}
	if(sig_info->si_code | CLD_STOPPED) {
		
		printf("CLD_STOPPED\n");
	}
#endif
	
	errno = err;
	return NULL;
}

//-----------------------------------------------------------------------------

/* The shell object plugins use.
 * Some methods are set to defaults.
 */
struct esh_shell shell =
{
    .build_prompt = build_prompt_from_plugins,
    .readline = readline,       /* GNU readline(3) */ 
    .parse_command_line = esh_parse_command_line, /* Default parser */
    .get_jobs = get_jobs,
    .get_job_from_jid = get_job_from_jid,
    .get_job_from_pgrp = get_job_from_pgrp,
    .get_cmd_from_pid = get_cmd_from_pid
};

int jid;
int curr_job;
int prev_job;
int exit_stat = EXIT_SUCCESS;
struct list current_jobs;
struct termios * tty_state;

int
main(int ac, char *av[])
{
    int opt;
    list_init(&esh_plugin_list);
    list_init(&current_jobs);
    jid=1;
	tty_state = esh_sys_tty_init();
	
	esh_signal_sethandler(SIGCHLD, (sa_sigaction_t) sig_chld_handler);
	//esh_signal_sethandler(SIGTSTP, (sa_sigaction_t) sig_chld_handler);
	if(signal(SIGTTOU, SIG_IGN) == SIG_ERR) {
		esh_sys_fatal_error("signal: SIGTTOU: ");
	}
	if(signal(SIGTTIN, SIG_IGN) == SIG_ERR) {
		esh_sys_fatal_error("signal: SIGTTOU: ");
	}
	/*if(signal(SIGTSTP, SIG_IGN) == SIG_ERR) {
		esh_sys_fatal_error("signal: SIGTTOU: ");
	}
	if(signal(SIGQUIT, SIG_IGN) == SIG_ERR) {
		esh_sys_fatal_error("signal: SIGTTOU: ");
	}
	if(signal(SIGINT, SIG_IGN) == SIG_ERR) {
		esh_sys_fatal_error("signal: SIGTTOU: ");
	}*/

    /* Process command-line arguments. See getopt(3) */
    while ((opt = getopt(ac, av, "hp:")) > 0) {
        switch (opt) {
        case 'h':
            usage(av[0]);
            break;

        case 'p':
            esh_plugin_load_from_directory(optarg);
            break;
        }
    }

    esh_plugin_initialize(&shell);

    /* Read/eval loop. */
    for (;;) {
        /* Do not output a prompt unless shell's stdin is a terminal */
        char * prompt = isatty(0) ? shell.build_prompt() : NULL;
        char * cmdline = shell.readline(prompt);
        free (prompt);

        if (cmdline == NULL)  /* User typed EOF */
            break;

        struct esh_command_line * cline = shell.parse_command_line(cmdline);
        free (cmdline);
        if (cline == NULL)                  /* Error in command line */
            continue;

        if (list_empty(&cline->pipes)) {    /* User hit enter */
            esh_command_line_free(cline);
            continue;
        }
		
#ifdef _DEBUG
        esh_command_line_print(cline);
#endif

	    process_cline(cline);

        esh_command_line_free(cline);
    }
	
	free_jobs(&current_jobs);
	
    return exit_stat;
}

void free_jobs(struct list * jobs) {
    struct list_elem * e = list_begin (jobs); 

    for (; e != list_end (jobs); ) {
        struct esh_pipeline *pipe = list_entry(e, struct esh_pipeline, elem);
        e = list_remove(e);
        esh_pipeline_free(pipe);
    }
}

void process_cline(struct esh_command_line *cline) {

	struct list_elem * e;
	int* pipefd = NULL;
	int i;

	while (!list_empty (&cline->pipes)) {
		e = list_pop_front(&cline->pipes);

        struct esh_pipeline *pipeline = list_entry(e, struct esh_pipeline, elem);
		int numberOfCommands = list_size(&pipeline->commands);
#ifdef _DEBUG
		printf("Num cmds: %d\n", numberOfCommands);
#endif
		//setup pipe structure
		if(numberOfCommands > 1) {
			pipefd = (int*)malloc(2*(numberOfCommands - 1) * sizeof(int));
			if(pipefd == NULL) {
				esh_sys_fatal_error("malloc: ");
			}
		}
		for(i = 0; i < 2*(numberOfCommands-1); i+=2) {
			esh_sys_call(pipe(pipefd+i), false,"pipe: ");
		}

		struct list_elem * element = list_begin(&pipeline->commands);
		struct  esh_command *cmds = list_entry(element, struct esh_command, elem);		

		//configure pipeline information
		if (!(strcmp(cmds->argv[0],"stop")==0 || strcmp(cmds->argv[0], "kill")==0 ||  strcmp(cmds->argv[0],"jobs")==0 || strcmp(cmds->argv[0],"fg")==0 || strcmp(cmds->argv[0],"bg") == 0))
		configure_pipeline(pipeline);
		
		//struct list_elem * element = list_begin(&pipeline->commands);
		for(i = -1; element != list_end(&pipeline->commands); element = list_next(element), i += 2) {
			
			cmds = list_entry(element, struct esh_command, elem);

			 // Minimum needed: jobs, fg, bg, kill, stop
		
			if (strcmp(cmds->argv[0],"jobs")==0)
			{
				struct list_elem *liel = list_begin(&current_jobs);
				while (liel != list_end(&current_jobs))
				{
					struct esh_pipeline *pl = list_entry(liel, struct esh_pipeline, elem);
					struct list_elem *liele = list_begin(&pl->commands);
					while (liele != list_end(&pl->commands)){
						struct esh_command *comnd = list_entry(liele, struct esh_command, elem);
						//printf("HERE\n");
						print_job_status(comnd);
						liele = list_next(liele);
					}
					liel = list_next(liel);	
				}
			}
			else if(strcmp(cmds->argv[0],"fg")==0 || strcmp(cmds->argv[0],"bg")==0 || 
				strcmp(cmds->argv[0], "kill")==0 || strcmp(cmds->argv[0], "stop")==0)
			{

				// Set up for identifying which job on which to call the following commands.
				if (!list_empty(&current_jobs))
				{
					int job_id = curr_job;
					if (cmds->argv[1] != NULL) //command was called without job specification
					{
						job_id = atoi(cmds->argv[1]);
					}
#ifdef _DEBUG
					printf("jid: %d\n", job_id);
#endif
					struct esh_pipeline *pl = get_job_from_jid(job_id);
					
					if(pl == NULL) {
						printf("esh: %s: %d: no such job\n", cmds->argv[0], jid);
					}
					else if (strcmp(cmds->argv[0],"fg")==0)
					{
#ifdef _DEBUG
						printf("Resuming job: %d in foreground\n", pl->pgrp);
#endif
						struct esh_command * cmd_to_resume = get_cmd_from_pid(pl->pgrp);
						char** args = cmd_to_resume->argv;
						printf("%s", *args);
						args++;
						while(*args) {
							printf(" %s", *args);
							args++;
						}
						printf("\n");
						
						put_job_in_foreground(pl, true, pl->pgrp);
					}
					else if (strcmp(cmds->argv[0],"bg")==0)
					{
#ifdef _DEBUG
						printf("Resuming job: %d in background\n", pl->pgrp);
#endif
						put_job_in_background(pl, true);
					} 
					else if (strcmp(cmds->argv[0], "kill")==0)
					{
						esh_sys_call(kill(-1* pl->pgrp, SIGKILL), true, "kill: ");
						list_remove(&pl->elem);
						esh_pipeline_free(pl);
					}
					else if (strcmp(cmds->argv[0], "stop")==0)
					{
						esh_sys_call(kill(-1 * pl->pgrp, SIGSTOP), true, "kill: ");
					}
				}
			} else if(strcmp(cmds->argv[0], "exit")==0) {
				if(cmds->argv[1] != NULL) {
					exit_stat = atoi(cmds->argv[1]);
				}
				esh_sys_call(close(STDIN_FILENO), true, "close: ");
			}
			else
			{
				setup_and_run_cmd(cmds, i<=0?NULL:pipefd+i -1, i+1 >= 2*(numberOfCommands-1)?NULL:pipefd+i+2);
			}
		}

		//for(i = 0; i < 2*(numberOfCommands-1); i++){
		//	esh_sys_call(close(pipefd[i]), false, "close: ");
		//}

		if(numberOfCommands > 1) {
			free(pipefd);
		}
	}
}

void configure_pipeline(struct esh_pipeline * pipeline) {
	int jbd =jid;
	prev_job = curr_job;
	curr_job = jbd;
	pipeline->jid = jid ++;
	list_push_back(&current_jobs, &pipeline->elem);
}

void setup_and_run_cmd(struct  esh_command *cmds, int* in_fd, int* out_fd) {
	
#ifdef _DEBUG
	if(in_fd != NULL) {
		printf("in_fd: %d\n", *in_fd);
	}
	if(out_fd != NULL) {
		printf("out_fd: %d\n", *out_fd);
	}
#endif

	cmds->pid = esh_sys_call(fork(), true, "fork: ");

	esh_sys_call(setpgid(cmds->pid,cmds->pid), false, "setpgid: ");
	cmds->pipeline->pgrp = getpgid(cmds->pid);
	
	if(cmds->pid == 0) {
		launch_process(cmds, in_fd, out_fd);
	} else {
		
		if(in_fd != NULL) {
			esh_sys_call(close(*in_fd), true, "close: ");
		}
		if(out_fd != NULL) {
			esh_sys_call(close(*out_fd), true, "close: ");
		}
		
		if(cmds->pipeline->bg_job) {
			put_job_in_background(cmds->pipeline, false);
		} else {
			put_job_in_foreground(cmds->pipeline, false, cmds->pid);
		}
	}
}

static void wait_for_job(struct esh_pipeline *job){
	int status;
	pid_t pid;

	/*
	do pid = waitpid(WAIT_ANY, &status, WUNTRACED);
	while (!mark_process_status(pid, status));		
	*/
	if ((pid = esh_sys_call(waitpid(-1, &status, WUNTRACED), true, "waitpid: ")) > 0){
		mark_process_status(pid, status);
		give_tty_to_pgrp(getpgrp(), tty_state, NULL);	
	}
}

static void put_job_in_foreground(struct esh_pipeline *job, bool cont, int pid){
	job->status = FOREGROUND;
	
	esh_sys_tty_save(tty_state);
	esh_sys_call(tcsetpgrp(esh_sys_tty_getfd(), pid), true, "tcsetpgrp: ");
	
	if (cont){
		esh_sys_tty_save(tty_state);
		esh_sys_call(kill(-job->pgrp, SIGCONT), true, "kill: ");}
		
	wait_for_job(job);
	
	esh_sys_call(tcsetpgrp(esh_sys_tty_getfd(), getpid()), true, "tcsetpgrp: ");
	esh_sys_tty_restore(tty_state);
} 

static void put_job_in_background(struct esh_pipeline *job, bool cont){
	job->status = BACKGROUND;
	if (cont) esh_sys_call(kill(-job->pgrp, SIGCONT), true, "kill: " );
	printf("[%d] %d\n", job->jid, job->pgrp);
}

void launch_process(struct  esh_command *cmds, int* in_fd, int* out_fd) {
#ifdef _DEBUG
		printf("CHILD: ");
		printf("%d %d %s", getpid(), getpgrp(),cmds->argv[0]);
		printf("\n");
#endif
		if(cmds->iored_input != NULL) {
			if(in_fd != NULL) {
				esh_sys_call(close(*in_fd), true, "close: ");
			}
			int fd = esh_sys_call(open(cmds->iored_input,O_RDONLY|O_CLOEXEC), true, "open: ");
			esh_sys_call(dup2(fd, STDIN_FILENO), true, "dup2: ");
		} else if(in_fd != NULL) {
			esh_sys_call(dup2(*in_fd, STDIN_FILENO), true, "dup2: ");
			esh_sys_call(close(*in_fd), true, "close: ");
		}
		
		if(cmds->iored_output != NULL) {
			if(out_fd != NULL) {
				esh_sys_call(close(*out_fd), true, "close: ");
			}
			int fd = esh_sys_call(open(cmds->iored_output, O_WRONLY|O_CREAT|O_CLOEXEC|(cmds->append_to_output?O_APPEND:0), file_mode_mask), true, "open: ");
			esh_sys_call(dup2(fd,STDOUT_FILENO), true, "dup2: ");
		} else if (out_fd != NULL) {
			esh_sys_call(dup2(*out_fd, STDOUT_FILENO), true, "dup2: ");
			esh_sys_call(close(*out_fd), true, "close: ");
		}
		esh_sys_call(execvp(cmds->argv[0], cmds->argv), true, "execvp: %s: ", cmds->argv[0]);
}

#ifndef HTTP_H
#define HTTP_H
#include <stdbool.h>

#ifndef SERVER_NAME
#define SERVER_NAME "SysStat Webservice"
#endif
#define MAXHEADERS 128

typedef enum { v1_0, v1_1 } http_version_t;
typedef enum { GET } http_method_t;

// Memory blocks.
struct mem_blk {
        struct mem_blk *next;
        void *addr;
};

typedef struct http_header {
	char name[MAXLINE];
	char value[MAXLINE];
} http_header_t;

typedef struct http_request {
	char filename[MAXLINE];
	char cgi_args[MAXLINE];
	http_version_t version;
	http_method_t method;
	http_header_t headers[MAXHEADERS];
	int num_headers;
	bool close;
	bool error;
	int errnum;
	char cause[MAXLINE];
	char short_msg[MAXLINE];
	char long_msg[MAXLINE];
} http_request_t;

typedef struct http_response {
	char body[MAXBUF];
	char header[MAXBUF];
} http_response_t;

void handle_connection(int);
void init_service();
void destroy_service();
void set_root(char* root_dir);
void handle_relay_service(struct addrinfo *addr);

#endif

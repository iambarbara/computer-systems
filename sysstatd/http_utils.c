#include "csapp.h"
#include "http_utils.h"
#include "threadpool.h"
#include <time.h>
#include <stdio.h>

//Legal characters in a callback value
#define LEGAL_CHARS "ABCDEFGHIJLKMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789._"
#define BLOCK_SIZE 64*1024*1024

#define MIN_THREADS 10
#define MAX_THREADS 200
#define THRESHOLD 0

// OSX uses MAP_ANON instead of MAP_ANONYMOUS
#ifndef MAP_ANONYMOUS
#define MAP_ANONYMOUS MAP_ANON
#endif

struct thread_pool * pool = NULL;

struct mem_blk *memblks;
static int count = 0;

//timeout in seconds for a connection;
static const long tv_sec = 300;
static const long tv_usec = 0;

//root directory to serve files from
static char* root_dir;

/*
 * System status web service that publishes system's status as reported by
 * kernel via the /proc/loadavg file system. Provides info  in JSON[1] format.
 */
void loadavg(char* json)
{
	char buff[MAXLINE];
	FILE *lfile = Fopen("/proc/loadavg", "r");
	Fgets(buff, MAXLINE, lfile);
	Fclose(lfile);

	char *load1 = NULL,
	     *load2 = NULL,
	     *load3 = NULL,
	     *running = NULL,
	     *total = NULL,
	     *last = NULL;
	
	load1 = buff;
	strtok_r(load1, " ", &load2);
	strtok_r(load2, " ", &load3);
	strtok_r(load3, " ", &running);
	strtok_r(running, "/", &total);
	strtok_r(total, " ", &last);

	sprintf(json, "{\"total_threads\":\"%s\", \"loadavg\": [\"%s\", \"%s\", \"%s\"], \"running_threads\": \"%s\"}\n", 
			total, load1, load2, load3, running);
}

/*
 * Forces the system to deallocate a chunk of physical memory. If all pages of
 * the virtual memory block are still present in physical memory at that time,
 * the expected result is an increase in the amount of physical memory.
 */
static void freeanon(char* buff)
{
	struct mem_blk *blk = memblks;
	*buff = '\0';

	if (memblks){
		memblks = memblks->next;
		munmap(blk->addr, BLOCK_SIZE);
		count--;
		sprintf(buff, "Unmapped 64MB, %d blocks left.", count);
	} else {
		sprintf(buff, "No blocks allocated.");
	}
}

/*
 * Forces the system to allocate physical memory devoted to anonymous virtual
 * memory. Each call to allocanon should result in the allocation of 64MB.
 */
static void allocanon(char* buff)
{
        struct mem_blk *blk = malloc(sizeof(struct mem_blk));
        blk->addr = mmap(0, BLOCK_SIZE, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0);

	count++;
        int i=1;
        while (i<BLOCK_SIZE-1) {
                ((char *)blk->addr)[i]='a';
                i+=100;
        }
        blk->next = memblks;
        memblks = blk;

        *buff = '\0';
        sprintf(buff, "Mapped and touched anonymous 64MB, now %d blocks allocated.",count);
}


/*
 * When URL is visited, runloop is called. Starts a process that loops for 
 * 15 seconds. Expected result is a temporary increase in load average.
 */
void* runloop()
{
	printf("Starting loop\n");
        long time = (clock()/(CLOCKS_PER_SEC/1000)) + 15000;
        while ((clock()/(CLOCKS_PER_SEC/1000))<time); //loop for 15 seconds
	printf("Ending loop\n");
	return NULL;
}


/*
 * System status web service that publishes system's status as reported by
 * kernel via the /proc/meminfo file system. Provides info  in JSON[1] format.
 */
char* meminfo()
{
	char json[MAXLINE] = "{";
        char line[200];
	int boolean = 0;

	FILE *mfile = Fopen("/proc/meminfo", "r");

	while (Fgets(line, sizeof(line), mfile))
	{
		if (boolean!=0) strcat(json, ",");
		char buffa[64], buffb[200], buffc[200];
		sscanf(line, "%s %s %s", buffa, buffc, buffb);
		buffa[strlen(buffa)-1]='\0';	
		char buffd[MAXLINE];
		sprintf(buffd, "\"%s\": \"%s\"", buffa, buffc);
		strcat(json, buffd);
		boolean = 1;
	}

	strcat(json, "}");
	Fclose(mfile);
	return strdup(json);
}

/*
 * Sets root directory.
 */
void set_root(char* dir) {
	root_dir = dir;
}

//Sends an http response on the fd associated with rio
static void send_response(int fd, http_response_t* response){
//	printf("%s", response->header);
//	printf("%s", response->body);
	Rio_writen(fd, response->header, strlen(response->header));
	Rio_writen(fd, response->body, strlen(response->body));
}

//Creates an http response with the default fields filled in.
static http_response_t * default_response(http_request_t *request) {
	http_response_t *response = malloc(sizeof(http_response_t));
	sprintf(response->header, "HTTP/1.1 %d %s\r\n", request->errnum, request->short_msg);
	sprintf(response->header, "%sServer: %s\r\n", response->header, SERVER_NAME);
	const time_t t = time(NULL);
	struct tm* tm = gmtime(&t);
	char date[MAXLINE];
	strftime(date, MAXLINE, "%a, %d %b %Y %T GMT", tm);
	sprintf(response->header, "%sDate: %s\r\n", response->header, date);
	if(request->close)
		sprintf(response->header, "%sConnection: Close\r\n", response->header);
	return response;
}

//Sends an error response to the client based on the error code and short/long message in request
static void client_error(int fd, http_request_t* request) {
	http_response_t* response = default_response(request);
	sprintf(response->body, "<html><head><title>Error Response</title></head>"\
				"<body bgcolor=\"#ffffff\"><h1>Error response</h1><p>Error code %d</p>"\
				"<p>Message: %s (%s).</p><p>Error code explination: %d = %s.</p></body></html>\r\n", 
				request->errnum, request->short_msg, request->cause, request->errnum, request->long_msg);
	sprintf(response->header, "%sContent-Type: text/html\r\nContent-Length: %zu\r\n\r\n", response->header, strlen(response->body));
	send_response(fd, response);
	free(response);
}

//Parses a header into an http_header_t object
static http_header_t parse_header(char* header_line) {
	http_header_t header;
	size_t len = strlen(header_line);
	header_line[len-1] = '\0';
	header_line[len-2] = '\0';
	char* pos = strstr(header_line, ": ");
	if(pos != NULL) {
		pos[0] = '\0';
		strncpy(header.name, header_line, MAXLINE);
		strncpy(header.value, pos + 2, MAXLINE);
	} else {
		header.name[0] = '\0';
		header.value[0] = '\0';
	}
	return header;
}

//splits a uri into the query arguments and filename, and sets the appropriate fields in request
static void parse_uri(char* uri, http_request_t *request) {
	char* ptr = index(uri, '?');
	
	if(ptr) {
		*ptr = '\0';
		ptr++;
		strcpy(request->cgi_args, ptr);
	} else {
		strcpy(request->cgi_args, "");	
	}
	strncpy(request->filename, uri, MAXLINE);
}

//reads the request headers from rio into the request object
static int read_requesthdrs(int fd, http_request_t *request) {
	char buf[MAXLINE];
	int res = Rio_readline(fd, buf, MAXLINE);
	while(res > 0 && strcmp(buf, "\r\n") && request->num_headers < MAXHEADERS) {
		http_header_t head = parse_header(buf);
		if(head.name[0] != '\0') {
			request->headers[request->num_headers++] = head;
		}
		res = Rio_readline(fd, buf, MAXLINE);
	}
	return res;
}

//Checks if the request contains a request to close the connection in the headers
static bool request_contains_close(http_request_t* request) {
	int i;
	for( i = 0; i < request->num_headers; i++) {
//		printf("header: %s = %s\n", request->headers[i].name, request->headers[i].value);
		if(!strcasecmp(request->headers[i].name, "Connection") && !strcasecmp(request->headers[i].value, "Close")) {
//			printf("Contains close header\n");
			return true;
		}
	}
	return false;
}

//Reads a request from the stream
static http_request_t *read_request(int fd) {
	http_request_t *request = malloc(sizeof(http_request_t));
	char buf[MAXLINE], method[MAXLINE], uri[MAXLINE], version[MAXLINE];

	int res = Rio_readline(fd, buf, MAXLINE);
//	printf("Received line: %s\n", buf);
	if(res <= 0) {
		request->error = true;
		request->errnum = -1;
	} else if(read_requesthdrs(fd, request) <= 0) {
		request->error = true;
		request->errnum = -1;
	} else if(sscanf(buf, "%s %s %s", method, uri, version) != 3) {
		request->error = true;
		request->errnum = 400;
		strcpy(request->short_msg, "Bad Request");
		strcpy(request->short_msg, "Bad syntax in request string");
		strncpy(request->cause, buf, MAXLINE);
	} else {

		if(!strcmp(method, "GET")) {
			request->method = GET;
		} else {
			request->error = true;
			request->errnum = 501;
			strcpy(request->short_msg, "Unsupported method");
			strcpy(request->long_msg, "Server does not suport this operation");
			strncpy(request->cause, method, MAXLINE);
		}

		if(!strcmp(version, "HTTP/1.1")) {
			request->version = v1_1;
		} else if (!strcmp(version, "HTTP/1.0")) {
			request->version = v1_0;
		} else {
			request->error = true;
			request->errnum = 400;
			strcpy(request->short_msg, "Bad request");
			strcpy(request->long_msg, "Bad request version supplied");
			strncpy(request->cause, version, MAXLINE);
		}
		parse_uri(uri, request);
	}
	request->close = request_contains_close(request) || request->version == v1_0 || request->error;
	return request;
}

//validates a callback based on the string of legal chars
static char* validate_callback(char* callback) {
	if(strspn(callback, LEGAL_CHARS) < strlen(callback) && strlen(callback) > 0) {
		return NULL;
	}
	return callback;
}

//Gets the first argument with name "callback"
static char* get_callback(char* cgi_args) {
	char* saveptr = NULL;
	char* arg = strtok_r(cgi_args, "&", &saveptr);
	while(arg) {
		if(sscanf(arg, "callback=%s", arg) == 1) {
			arg=validate_callback(arg);
			if(arg != NULL) break;
		}
		arg = strtok_r(NULL, "&", &saveptr);
	}
	return arg;
}

//sends a json string in response on this open fd
static void send_json(int  fd, char* json, http_request_t* request) {
	http_response_t *response = default_response(request);
	char* callback = get_callback(request->cgi_args);
	if(callback != NULL) {
		sprintf(response->body, "%s(%s)", callback, json);
	} else {
		sprintf(response->body, "%s", json);
	}
	sprintf(response->header, "%sContent-Type: application/json\r\nContent-Length: %zu\r\n\r\n", response->header, strlen(response->body));
	send_response(fd, response);
	free(response);
}
//handles a request sent to /loadavg
static void process_loadavg(int fd, http_request_t * request) {
	char load[MAXLINE];
	loadavg(load);
	send_json(fd, load, request);
}
//handles a request sent to /meminfo
static void process_meminfo(int fd, http_request_t* request) {
	char* info = meminfo();
	send_json(fd, info, request);
	free(info);
}
//gets a file type (for Content-Type header) based on its extension.
//If it is not one of (html, gif, jpg, js, css) it is assumed to be plain
//text
static void get_filetype(char* filename,char* filetype) {
	if (strstr(filename, ".html"))
		strcpy(filetype, "text/html");
	else if (strstr(filename, ".gif"))
		strcpy(filetype, "image/gif");
	else if (strstr(filename, ".jpg"))
		strcpy(filetype, "image/jpeg");
	else if (strstr(filename, ".js"))
		strcpy(filetype, "text/javascript");
	else if (strstr(filename, ".css"))
		strcpy(filetype, "text/css");
	else
		strcpy(filetype, "text/plain");
}
//handles a request for a static file.
static void process_static(int fd, http_request_t* request) {
	struct stat sbuf;
	char abs_path [MAXLINE];
	strncpy(abs_path, root_dir, MAXLINE);
	strncat(abs_path, request->filename, MAXLINE - strlen(abs_path));
	if(abs_path[strlen(abs_path) - 1] == '/') {
		strncat(abs_path, "index.html", MAXLINE - strlen(abs_path));
	}

	if(stat(abs_path, &sbuf) < 0) {
		request->errnum = 404;
		strcpy(request->short_msg, "Not Found");
		strcpy(request->long_msg, "Requested resource was not found");
		strncpy(request->cause, abs_path, MAXLINE);
		client_error(fd, request);
	} else if (!(S_ISREG(sbuf.st_mode)) || !(S_IRUSR & sbuf.st_mode)) {
		request->errnum = 404;
		strcpy(request->short_msg, "Not Found");
		strcpy(request->long_msg, "Requested resource is forbidden");
		strcpy(request->cause, abs_path);
		client_error(fd, request);
	} else {
		request->errnum = 200;
		strcpy(request->short_msg, "Ok");
		int srcfd = Open(abs_path, O_RDONLY, 0);
		char* srcp = Mmap(0, sbuf.st_size, PROT_READ, MAP_PRIVATE, srcfd, 0);
		Close(srcfd);
		char filetype[MAXLINE];
		get_filetype(abs_path, filetype);
		http_response_t * response = default_response(request);
		sprintf(response->header, "%sContent-Length: %zu\r\nContent-Type: %s\r\n\r\n", response->header, sbuf.st_size, filetype);
		Rio_writen(fd, response->header, strlen(response->header));
		Rio_writen(fd, srcp, sbuf.st_size);
		Munmap(srcp, sbuf.st_size);
		free(response);
	}
}
//sends a plain text response (such as for /runloop)
static void send_plain(char* plain_text, int fd, http_request_t* request) {
       	http_response_t* response = default_response(request);
	sprintf(response->body, "%s", plain_text);
	sprintf(response->header, "%sContent-Type: text/plain\r\nContent-Length: %zu\r\n\r\n", response->header, strlen(response->body));
	send_response(fd, response);
       	free(response);
}
//handles a request to /allocanon
static void process_allocanon(int fd, http_request_t *request) {
	char buff[MAXLINE];
	allocanon(buff);
	send_plain(buff, fd, request);
}
//handles a request to /freeanon
static void process_freeanon(int fd, http_request_t *request) {
	char buff[MAXLINE];
	freeanon(buff);
	send_plain(buff, fd, request);
}
//handles a request to /runloop
static void process_runloop(int fd, http_request_t *request) {
	thread_pool_submit(pool, (thread_pool_callable_func_t) runloop, NULL);
	send_plain("Started 15 second spin.", fd, request);
}
//determines what type of request this is, and delegates to the correct function
static void handle_request(int fd, http_request_t * request) {
	printf("Filename: %s\n", request->filename);
	if(!strcmp(request->filename, "/loadavg")) {
		request->errnum = 200;
		strcpy(request->short_msg, "Ok");
		process_loadavg(fd, request);
	} else if(!strcmp(request->filename, "/meminfo")) {
		request->errnum = 200;
		strcpy(request->short_msg, "Ok");
		process_meminfo(fd, request);
	} else if(!strcmp(request->filename, "/freeanon")) {
		request->errnum = 200;
		strcpy(request->short_msg, "Ok");
		process_freeanon(fd, request);
	} else if(!strcmp(request->filename, "/allocanon")) {
		request->errnum = 200;
                strcpy(request->short_msg, "Ok");
		process_allocanon(fd, request);
	} else if(!strcmp(request->filename, "/runloop")) {
                request->errnum = 200;
                strcpy(request->short_msg, "Ok");
		process_runloop(fd, request);
	} else { //assume static content
		process_static(fd, request);
	}
}

/*
 * Handles all requests on a connection. Will timeout after the specified amount of time.
 * If it recieves an HTTP/1.0 request, it will close the connection immediately.
 */
static void* handle_requests_on_connection(void* descriptor) {
	int fd = *((int*)descriptor);
	Free(descriptor);

	struct timeval timeout;
	timeout.tv_sec = tv_sec;
	timeout.tv_usec = tv_usec;
	printf("Handling requests on connection with fd: %d\n", fd);
	fd_set set;

	FD_ZERO(&set);
	FD_SET(fd, &set);

	Select(fd+1, &set, NULL, NULL, &timeout);
	while(FD_ISSET(fd, &set)) {
		timeout.tv_sec = tv_sec;
		timeout.tv_usec = tv_usec;
		http_request_t *request = read_request(fd);
		if(request->error) {
//			printf("Error, number: %d\n", request->errnum);
			if(request->errnum > 0) {
				client_error(fd, request);
			}
		} else {
			handle_request(fd, request);
		}
		if(request->close) 
			break;
		Free(request);
		if(FD_ISSET(fd, &set)) {
			Select(fd+1, &set, NULL, NULL, &timeout);
		} else {
//			printf("Closing connection: %d\n", fd);
			break;
		}
	}
	Close(fd);
	return NULL;
}
//initializes http server stuff
void init_service() {
	pool = thread_pool_new_dynamic(MIN_THREADS, MAX_THREADS, THRESHOLD);
}
//destroys http server stuff
void destroy_service() {
	thread_pool_shutdown(pool);
}
//spawns a new thread to handle the connection on this fd
void handle_connection(int fd) {
	int* fd_ptr = Malloc(sizeof(int));
	*fd_ptr = fd;
	thread_pool_submit(pool, (thread_pool_callable_func_t) handle_requests_on_connection, (void *) fd_ptr);
}
//restarts the connection to the relay server if it closes
//delegates to handle_requests_on_connection to process everything
//from the relay server
static void* relay_service_wrapper(void* addr_ptr) {
	struct addrinfo* addr = (struct addrinfo*)addr_ptr;
	
	char prefix[MAXLINE] = "group314\r\n";

	while(1) {
		int* fd_ptr = Malloc(sizeof(int));
		*fd_ptr = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol);
		if(*fd_ptr == -1)
			break;
		if(connect(*fd_ptr, addr->ai_addr, addr->ai_addrlen) == -1)
			break;
		Rio_writen(*fd_ptr, prefix, strlen(prefix));
//		printf("Sent prefix: %s to relay server on fd: %d\n", prefix, *fd_ptr);
		handle_requests_on_connection(fd_ptr);
	}
	printf("RELAYING DONE =(\n");
	return NULL;
}
//spawns a new thread to connect to a relay server.
void handle_relay_service(struct addrinfo* addr) {
	thread_pool_submit(pool, (thread_pool_callable_func_t) relay_service_wrapper, (void *)addr);
}

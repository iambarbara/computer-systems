#include <errno.h>
#include <netdb.h>
#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/mman.h>
#include "threadpool.h"
#include "csapp.h"
#include "http_utils.h"

#include <netinet/in.h>
#include <sys/poll.h>

#define DEF_PORT "14475"

//struct mem_blk *memblks;

/*
 * Main method for program.
 */
int main(int argc, char *argv[])
{
	init_service();

  	char* port = DEF_PORT;
	char* relay_host = NULL;
	char* root_dir = NULL;
	//Process cmd arguments
	int c;
	
	while((c = getopt(argc, argv, "p:r:R:")) != -1) {
		switch(c) {
		case 'p':
			port = optarg;
			break;
		case 'r':
			relay_host = optarg;
			break;
		case 'R':
			root_dir = optarg;
			break;
		default:
			printf("usage: %s [-p port] [-r relayhost:port] [-R path]\n", argv[0]);
			exit(EXIT_FAILURE);
		}
	}
	//if we didn't receive a -R, then setup the defaut root directory as "."
	if(root_dir == NULL) {
		root_dir = ".";
	}
	set_root(root_dir);

	struct addrinfo *rp;
	if(relay_host != NULL) {
		char* port = NULL;
		strtok_r(relay_host, ":", &port);
		//start up relay server connection
		struct addrinfo *ri;
		struct addrinfo relay_hints;
		memset (&relay_hints, '\0', sizeof (relay_hints));
		relay_hints.ai_family = AF_INET;
		relay_hints.ai_flags = AI_NUMERICSERV;
		relay_hints.ai_socktype = SOCK_STREAM;
		int e = getaddrinfo (relay_host, port, &relay_hints, &ri);
		if( e != 0 ) {
			printf("getaddrinfo: %s\n", gai_strerror(e));
		} else {
			int fd = -1;
			for (rp = ri; rp != NULL; rp = rp->ai_next) {
				fd = socket (ri->ai_family, ri->ai_socktype, ri->ai_protocol);
				if(fd == -1) {
					perror("socket: ");
					continue;
				}
				if(connect(fd, rp->ai_addr, rp->ai_addrlen) != -1) {
					break;
				}
				perror("connect: ");
				Close(fd);
				
			}
			if(rp == NULL) {
				printf("Could not connect to relay server\n");
				exit(EXIT_FAILURE);
			}
			Close(fd);
			handle_relay_service(rp);
		}
	}
	//setup the stuff needed to actually get the interfaces which are availible to receive connections on.
	struct addrinfo *ai;
	struct addrinfo hints;
	memset (&hints, '\0', sizeof (hints));
	hints.ai_flags = AI_PASSIVE | AI_ADDRCONFIG | AI_NUMERICSERV | AI_V4MAPPED | AI_ALL;
	hints.ai_socktype = SOCK_STREAM;
	int e = getaddrinfo (NULL, port, &hints, &ai);
	if (e != 0) {
		printf("getaddrinfo: %s", gai_strerror(e));
		exit(EXIT_FAILURE);
	}

	int nfds = 0;
	struct addrinfo *runp = ai;
	bool contains_v6 = false;
	//go through list of addrinfo and get a count of all of them. also determine if there are any ipv6 addresses
	while (runp != NULL) {
		++nfds;
		contains_v6 = contains_v6 || runp->ai_family == AF_INET6;
		runp = runp->ai_next;
	}
	struct pollfd fds[nfds];
	//go through the addresses and attempt to bind to each one. if it receives an address already in use error, then close the fd and continue.
	//else, panic and exit the program
	for (nfds = 0, runp = ai; runp != NULL; runp = runp->ai_next) {
		if(runp->ai_family == AF_INET && contains_v6) {
			printf("skipping IPV4 addr connection\n");
			continue;
		}
		fds[nfds].fd = socket (runp->ai_family, runp->ai_socktype, runp->ai_protocol);
		if (fds[nfds].fd == -1) {
			perror("socket: ");
		}
		fds[nfds].events = POLLIN;
		int opt = 1;
		setsockopt (fds[nfds].fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof (opt));
	
		if (bind (fds[nfds].fd, runp->ai_addr, runp->ai_addrlen) != 0) {
			perror("bind: ");
			if (errno != EADDRINUSE) {
				exit(EXIT_FAILURE);
			}
			close (fds[nfds].fd);
		} else {
			if (listen (fds[nfds].fd, SOMAXCONN) != 0) {
				perror("listen: ");
				exit(EXIT_FAILURE);
			}
			++nfds;
		}
	}
	freeaddrinfo (ai);

	while (1) {
		//block on the fd array until on of the is ready for input
		int n = poll (fds, nfds, -1);
		if (n > 0) {
			int i;
			for (i = 0; i < nfds; ++i) {
			//handle the input, spawn a new thread
				if (fds[i].revents & POLLIN) {
					struct sockaddr_storage rem;
					socklen_t remlen = sizeof (rem);
					int fd = accept (fds[i].fd, (struct sockaddr *) &rem, &remlen);
					if (fd != -1) {
						handle_connection(fd);
//						connection_t * data = malloc(sizeof(connection_t));
//						data->connfd = fd;
//						data->rem = rem;
					}
				}
			}
		}
	}
	//Clean errthang up
	destroy_service();
	freeaddrinfo(rp); 
	return 0;
}

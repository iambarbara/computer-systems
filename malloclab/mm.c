/*
* mm.c - The fastest, somewhat memory-efficient malloc package.
* -------------------------------------------------------------
* This program uses a segregated free list data structure
* with a first fit algorithm.
*/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>

#include "mm.h"
#include "memlib.h"

#if __x86_64__
typedef long point;
/* Read and write a word at address p */
#define GET(p)		(*(unsigned long *)(p))
#define PUT(p, val) 	(*(unsigned long *)(p) = (val))

/* Read the size from address p */
#define GET_SIZE(p)	(GET(p) & ~0x7)
#define HDRP(bp)	((int *)(bp) - 2)
#else
typedef int point;
/* Read and write a word at address p */
#define GET(p)		(*(unsigned int *)(p))
#define PUT(p, val) 	(*(unsigned int *)(p) = (val))

/* Read the size from address p */
#define GET_SIZE(p)	(GET(p) & ~0x7)
#define HDRP(bp)	((int *)(bp) - 2)
#endif

static point* free_list; /* Pointer to the beginning of the free list*/
int mm_check(void);

/*********************************************************
 * NOTE TO STUDENTS: Before you do anything else, please
 * provide your team information in the following struct.
 ********************************************************/
team_t team = {
    /* Team name */
    "antidisestablishmentarianism",
    /* First member's full name */
    "Barbara Brown",
    /* First member's SLO (@cs.vt.edu) email address */
    "brownba1",
    /* Second member's full name (leave blank if none) */
    "Lloyd Ramey",
    /* Second member's SLO (@cs.vt.edu) email address (leave blank if none) */
    "lnr0626"
};


/* 
 * mm_init - initialize the malloc package.
 */
int mm_init(void) {
	if ((free_list = mem_sbrk(14 * sizeof(void*))) == (void*) - 1) return -1;

	PUT(free_list + 0, 0);   //32
	PUT(free_list + 1, 0);   //64
	PUT(free_list + 2, 0);   //128
	PUT(free_list + 3, 0);   //256
	PUT(free_list + 4, 0);   //512
	PUT(free_list + 5, 0);   //1024
	PUT(free_list + 6, 0);   //2048 
	PUT(free_list + 7, 0);   //4096
	PUT(free_list + 8, 0);   //8192
	PUT(free_list + 9, 0);   //16384
	PUT(free_list + 10, 0);   //24576
	PUT(free_list + 11, 0);   //32768
	PUT(free_list + 12, 0);  //65536
	PUT(free_list + 13, 0);  //614784

	return 0;
}

/* 
 * mm_malloc - Allocate a block by incrementing the brk pointer.
 *  Always allocate a block whose size is a multiple of the alignment.
 */
void *mm_malloc(size_t size) {
	point index; 
	point* newptr;

	if(size <= 32)		{index = 0; size = 32;}
	else if(size <= 64)	{index = 1; size = 64;}
	else if (size <= 128)	{index = 2; size = 128;} 
	else if (size <= 256)	{index = 3; size = 256;} 
	else if (size <= 512)	{index = 4; size = 512;} 
	else if (size <= 1024)	{index = 5; size = 1024;} 
	else if (size <= 2048)	{index = 6; size = 2048;} 
	else if (size <= 4096)	{index = 7; size = 4096;} 
	else if (size <= 8192)	{index = 8; size = 8192;} 
	else if (size <= 16384) {index = 9; size = 16384;} 
	else if (size <= 24576)	{index = 10; size = 24576;} 
	else if (size <= 32768)	{index = 11; size = 32768;} 
	else if (size <= 65536) {index = 12; size = 65536;} 
	else if (size <= 614784) {index = 13; size = 614784;} 
	else return NULL;

	newptr = (void *)*(free_list + index);

	/* If free_list is empty, get a new block. Else, use free block */ 
	if (!newptr) {
		if ((newptr = mem_sbrk(size + 8)) == (void *) -1) return NULL;
		PUT(newptr, size);
	} else *(free_list + index) = *(newptr + 1);	
	
	//if(mm_check() != 0)
	//	exit(1);

	return newptr + 2;	
}

/*
 * mm_free - Returns the block to the freeblock list at the correct position (determined by address),
 *  then coallesses with the next and/or prev block if needed..
 */
void mm_free(void *ptr) {
	point index = -1;        
	point* listptr;

	/* Check the size and get the index of the free list */
	switch (GET_SIZE(HDRP(ptr))) {   
		case 32:  index = 0; break;
		case 64:  index = 1; break;
		case 128: index = 2; break;
		case 256: index = 3; break;			
		case 512: index = 4; break;			
		case 1024: index = 5; break;			
		case 2048: index = 6; break;			
		case 4096: index = 7; break; 			
		case 8192: index = 8; break; 			
		case 16384: index = 9; break;		
		case 24576: index = 10; break;					
		case 32768: index = 11; break;				
		case 65536: index = 12; break;			
		case 614784: index = 13; break;
	}
	
	/* If free_list is empty, first block ptr = 0. Else, point to prev pointer. */
	listptr = (free_list + index);    
	if (!*listptr) *((point**)ptr - 1) = 0;
	else *((point*)ptr - 1) = *listptr;
	*((point**)listptr) = (point*)ptr - 2; 

	//if(mm_check() != 0)
	//	exit(1);
}

/*
 * mm_realloc - Implemented simply in terms of mm_malloc and mm_free
 */
void *mm_realloc(void *oldptr, size_t size) {
	void *newptr;
	size_t asize;

	asize = *(size_t *)((int *) oldptr - 2);
	if (size > asize) {
		asize = size;
		newptr = mm_malloc(size);
		if (newptr == NULL)
			return NULL;
		memcpy(newptr, oldptr, asize);
		mm_free(oldptr);
		return newptr; 
	} else 
		return oldptr;
}

/*
 * mm_check_size - Returns the size of a block using a pointer.
 */
int mm_check_size(void* ptr) {
        switch (*((int*) ptr)) {
		case 32:  return 32;
		case 64:  return 64;
                case 128: return 128;
                case 256: return 256;
                case 512: return 512;
                case 1024: return 1024;
                case 2048: return 2048;
                case 4096: return 4096;
                case 8192: return 8192;
                case 16384: return 16384;
                case 24576: return 24576;
                case 32768: return 32768;
                case 65536: return 65536;
                case 614784: return 614784;
                default: return -1;
        }
}

/*
* mm_check - Checks free list and heap. 
*/

int mm_check(void) {
	int i, csize, blksize;
	int heap = 1;
	point* ptr;

	// Test free list
	for (i = 0; i < 12; i++) {
		switch (i) {
			case 0: blksize = 32;
			case 1: blksize = 64;
                	case 2: blksize = 128;
                	case 3: blksize = 256;
                	case 4: blksize = 512;
                	case 5: blksize = 1024;
                	case 6: blksize = 2048;
                	case 7: blksize = 4096;
                	case 8: blksize = 8192;
                	case 9: blksize = 16384;
                	case 10: blksize = 24576;
                	case 11: blksize = 32768;
                	case 12: blksize = 65536;
                	case 13: blksize = 614784;
                	default: blksize = -1;
        	}

		if (*(free_list + i) != 0) {
			ptr = (point*)*(free_list + i);
			while (ptr) {		

				//Check if block is aligned to 8 bytes.
				if (((point) ptr) % 2) {
					printf ("Warning: block at %p is not aligned correctly.\n", ptr);
					return 1;
				}

				//Check if size is valid. Also checks header.
				csize = mm_check_size ((void*) ptr);		
				if (csize == -1) {
					printf ("Warning: address at %p hasn't valid header.\n", ptr);
					return 1;
				}

				//Check that block size is correct and on correct list.
				if (csize != blksize) {
					printf ("Warning: block at %p doesn't go on free list.\n", ptr);
					return 1;
				}
				ptr = (point*)*(ptr + 1);		
			}
		}
	} 

	//Test the whole heap
	ptr = (free_list + 13);
	while (heap) {

		// Check byte alignment.
		if (((point) ptr) % 2) {
			printf ("Warning: byte at %p is not aligned correctly.\n", ptr);
			return 1;
		}

		// Make sure that the block is a valid size.
		if (mm_check_size((void*) ptr) == -1) {
			printf ("Warning: block at %p hasn't valid header.\n", ptr);
			return 1;
		}

		// Keep searching until the end of the heap.
		if (((point) ptr + *ptr + 8) < ((point)((point*) mem_heap_hi()))) 
			ptr = (point*)ptr + (*ptr / 4) + 2;
		else 
			heap = 0;
	}
	return 0;
}

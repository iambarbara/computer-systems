#include "list.h"
#include "threadpool.h"
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct thread_pool {
	int nthreads;
	pthread_t* threads;
	pthread_mutex_t work_list_mutex;
	sem_t work_availible;
	struct list /* future */ work;
} thread_pool_t;

//Holds the information needed to complete this piece of work. 
typedef struct future {
	thread_pool_callable_func_t callable;
	void * callable_data;
	void* data;
	bool complete;
	pthread_mutex_t mutex;
	pthread_cond_t completed;
	struct list_elem elem;
} future_t;

// used to free the list of futures which haven't been started.
static void free_unstarted_futures(struct thread_pool * pool);
static void* worker_thread(void* pool);

/* Create a new thread pool with n threads. */
struct thread_pool * thread_pool_new(int nthreads)
{
	struct thread_pool *pool = malloc(sizeof(struct thread_pool));
	pool->nthreads = nthreads;
	pool->threads = (pthread_t*) malloc(sizeof(pthread_t) * nthreads);
	sem_init(&pool->work_availible, 0, 0);
	pthread_mutex_init(&pool->work_list_mutex, NULL);
	
	int i;
	for(i = 0; i < nthreads; i++) {
		pthread_create(pool->threads+i, NULL, worker_thread, (void*) pool);
	}
	
	list_init(&pool->work);

	return pool;
}

// Threadpool thread function. It sets the cancel state of the pthread, casts the void* to
// struct thread_pool*, then goes into an infinite loop which waits until there is work, 
// completes that work, tests for cancellation, and yields the processor. The last step is
// probably unnecessary, but it makes sure the threads are playing nice.
void * worker_thread(void * p) {
	pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, NULL);
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	struct thread_pool* pool = (struct thread_pool*) p;
	while(true) {
		sem_wait(&pool->work_availible);
		
		pthread_mutex_lock(&pool->work_list_mutex);
		struct future * fut = list_entry(list_pop_front(&pool->work), struct future, elem);
		pthread_mutex_unlock(&pool->work_list_mutex);

		pthread_mutex_lock(&fut->mutex);
		fut->data = fut->callable(fut->callable_data);
		fut->complete = true;
		pthread_cond_broadcast(&fut->completed);
		pthread_mutex_unlock(&fut->mutex);
		

		pthread_testcancel();
		sched_yield();
	}
	return NULL;
}

/* Shutdown this thread pool.  Does not execute already queued, but unstarted tasks. It
	will finish any task that has already been started. */
void thread_pool_shutdown(struct thread_pool * pool)
{
	int i;
	for(i = 0; i < pool->nthreads; i++) {
		pthread_cancel(pool->threads[i]);
		pthread_join(pool->threads[i], NULL);
	}

	free_unstarted_futures(pool);
	pthread_mutex_destroy(&pool->work_list_mutex);
	sem_destroy(&pool->work_availible);
	free(pool->threads);
	free(pool);
}

// Goes through the list of queued futures, and frees them.
static void free_unstarted_futures(struct thread_pool* pool) {
	pthread_mutex_lock(&pool->work_list_mutex);
	while(!list_empty(&pool->work)) {
		struct future * future = list_entry(list_pop_front(&pool->work), struct future, elem);
		future_free(future);
	}
	pthread_mutex_unlock(&pool->work_list_mutex);
}

// Submits a new task to the thread pool.
struct future * thread_pool_submit( struct thread_pool* pool, thread_pool_callable_func_t callable,
					void * callable_data) {
	struct future * fut = (struct future*)malloc(sizeof(struct future));
	fut->callable = callable;
	fut->callable_data = callable_data;
	fut->data = NULL;
	fut->complete = false;
	pthread_cond_init(&fut->completed, NULL);
	pthread_mutex_init(&fut->mutex, NULL);

	pthread_mutex_lock(&pool->work_list_mutex);
	list_push_back(&pool->work, &fut->elem);
	pthread_mutex_unlock(&pool->work_list_mutex);

	sem_post(&pool->work_availible);
	return fut;
}

/* Make sure that thread pool has completed executing this callable,
 *  * then return result. */
void * future_get(struct future * fut)
{
	pthread_mutex_lock(&fut->mutex);
	while(!fut->complete) {
		pthread_cond_wait(&fut->completed, &fut->mutex);
	}
	pthread_mutex_unlock(&fut->mutex);
	void* data = fut->data;
	return data;
}

/* Deallocate this future.  Must be called after future_get() */
void future_free(struct future * fut)
{
	pthread_mutex_destroy(&fut->mutex);
	pthread_cond_destroy(&fut->completed);
	free(fut);
}

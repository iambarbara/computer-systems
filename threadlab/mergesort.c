/*
 * Parallel Quicksort.
 *
 * Demo application that shows how one might use threadpools/futures
 * in an application.
 *
 * Requires threadpool.c/threadpool.h
 *
 * Written by Godmar Back gback@cs.vt.edu for CS3214 Fall 2010.
 */

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <pthread.h>
#include <assert.h>
#include <getopt.h>

#include "threadpool.h"

typedef void (*sort_func)(int *, int);

/* Return true if array 'a' is sorted. */
static bool 
check_sorted(int a[], int n) 
{
    int i;
    for (i = 0; i < n-1; i++)
        if (a[i] > a[i+1])
            return false;
    return true;
}

/* ------------------------------------------------------------- 
 * utility routines.
 */
static inline int cmp_int(const int a, const int b)
{
    return a - b;
}

/* Swap two elements */
static inline void swap(int *a, int *b)
{
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

// maximum depth to which each recursive call is executed in parallel
static int depth = 3;
static int minrun = 2;

/* ------------------------------------------------------------- 
 * Serial implementation of mergesort.
 */
static void
merge_serial(int* result, int* temp, int left_pos, int left_stop, int right_pos, 
					int right_stop, int result_pos) {
					
	int result_stop = result_pos + left_stop - left_pos + right_stop - right_pos + 1;
	while(result_pos <= result_stop){
		if(left_pos > left_stop) {
			result[result_pos++] = temp[right_pos++];
		} else if(right_pos > right_stop) {
			result[result_pos++] = temp[left_pos++];
		} else {
			result[result_pos++] = cmp_int(temp[left_pos], temp[right_pos]) < 0
											? temp[right_pos++]
											: temp[left_pos++];
		}
	}
}

static void
insertion(int *array, int start, int end) {
	int i;
	int swap_pos;
	for(i = start; i <= end; i++) {
		swap_pos = i;
		while(swap_pos > start && array[swap_pos] < array[swap_pos-1]) {
			swap(array + swap_pos - 1, array + swap_pos);
			swap_pos--;
		}
	}
}

static void
mergesort_serial_internal(int *array, int* temp, int start, int end, int minrun) 
{
	if(end - start <= minrun) {
		insertion(array, start, end);
	} else {
		int mid = (start+end)/2;
		mergesort_serial_internal(array, temp,start, mid, minrun);
		mergesort_serial_internal(array, temp, mid+1, end, minrun);
		merge_serial(array, temp, start, mid, mid+1, end, start);
	}
}

static void
mergesort_serial(int* array, int N) {
	int* temp = malloc(sizeof(int) * N);
	mergesort_serial_internal(array, temp, 0, N-1, minrun);
	free (temp);
}

/* ------------------------------------------------------------- 
 * Parallel implementation.
 */

static struct thread_pool * threadpool;

typedef struct mergesort_task {
	int* array;
	int* temp;
	int start, end, depth;
	struct future* future;
} mergesort_task_t;

typedef struct merge_task {
	int* result;
	int* temp;
	int left_pos, left_stop, right_pos, right_stop, result_pos;
	struct future* future;
} merge_task_t;

static struct mergesort_task*
create_sort_task(int* array, int* temp, int start, int end) {
	struct mergesort_task* t = calloc(sizeof(struct mergesort_task), 1);
	t->array = array;
	t->temp = temp;
	t->start = start;
	t->end = end;
	return t;
}

static void
mergesort_parallel_internal(struct mergesort_task* task) {
	int* array = task->array;
	int* temp = task->temp;
	int start = task->start;
	int end = task->end;

	mergesort_serial_internal(array, temp, start, end, minrun);
}

static void
mergesort_parallel(int* array, int N) {
	int* temp = malloc(sizeof(int) * N);

	int size = N / depth;
	int start;
	struct mergesort_task** sort_tasks = calloc(sizeof(struct mergesort_task*), depth);
	int sub_array = 0;
	for(start = 0; start < N && sub_array < depth - 1; start += size) {
		sort_tasks[sub_array] = create_sort_task(array, temp, start, start+size);
		sort_tasks[sub_array]->future = thread_pool_submit(threadpool, 
				(thread_pool_callable_func_t) mergesort_parallel_internal, 
				sort_tasks[sub_array]);
		sub_array += 1;
	}

	sort_tasks[sub_array] = create_sort_task(array, temp, start, N-1);
	sort_tasks[sub_array]->future = thread_pool_submit(threadpool, 
				(thread_pool_callable_func_t) mergesort_parallel_internal, 
				sort_tasks[sub_array]);

	for(sub_array = 0; sub_array < depth; sub_array++) {
		future_get(sort_tasks[sub_array]->future);
		future_free(sort_tasks[sub_array]->future);
		free(sort_tasks[sub_array]);
	}
	
	free(sort_tasks);
	free(temp);
}

/*
 * Benchmark one run of sort_func sorter
 */
static void 
benchmark(const char *benchmark_name, sort_func sorter, int *a0, int N)
{
    struct timeval start, end, diff;

    int *a = malloc(N * sizeof(int));
    memcpy(a, a0, N * sizeof(int));

    gettimeofday(&start, NULL);
    sorter(a, N);
    gettimeofday(&end, NULL);

    if (!check_sorted(a, N)) {
        fprintf(stderr, "Sort failed\n");
        abort();
    }
    timersub(&end, &start, &diff);
    printf("%-20s took %.3f sec.\n", benchmark_name, diff.tv_sec + diff.tv_usec / 1.0e6);
    free(a);
}

static void
usage(char *av0, int minrun, int depth, int nthreads)
{
    fprintf(stderr, "Usage: %s [-l <n>] [-d <n>] [-n <n>] [-s <n>] [-m] <N>\n"
                    " -l        minimun run length, default %d\n"
                    " -d        number of tasks to split sort into, default %d\n"
                    " -n        number of threads in pool, default %d\n"
                    " -s        specify srand() seed\n"
                    " -m        don't run serial mergesort\n"
                    , av0, minrun, depth, nthreads);
    exit(0);
}

int 
main(int ac, char *av[]) 
{
	//struct thread_pool* threadpool;
    int nthreads = 4;
    int c;
	bool run_serial_mergesort = true;

    while ((c = getopt(ac, av, "d:n:hms:")) != EOF) {
        switch (c) {
        case 'l':
            minrun = atoi(optarg);
            break;
        case 'd':
            depth = atoi(optarg);
            break;
        case 'n':
            nthreads = atoi(optarg);
            break;
        case 's':
            srand(atoi(optarg));
            break;
		case 'm':
	    	run_serial_mergesort = false;
	    break;
        case 'h':
            usage(av[0], minrun, depth, nthreads);
        }
    }
    if (optind == ac)
        usage(av[0], minrun, depth, nthreads);
	
    int N = atoi(av[optind]);

    int i, * a0 = malloc(N * sizeof(int));
    for (i = 0; i < N; i++)
        a0[i] = random();

    if (run_serial_mergesort)
		benchmark("mergesort serial", mergesort_serial, a0, N);

    threadpool = thread_pool_new(nthreads);
    printf("Using %d threads, number of tasks to use=%d, minimun run length=%d\n", nthreads, depth, minrun);
    sleep(1);

    benchmark("mergesort parallel", mergesort_parallel, a0, N);

    thread_pool_shutdown(threadpool);
	
	free(a0);
    return 0;
}

